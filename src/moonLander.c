#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

const char *title =                                             
    ",-.-.               |                  |          \n"
    "| | |,---.,---.,---.|    ,---.,---.,---|,---.,---.\n"
    "| | ||   ||   ||   ||    ,---||   ||   ||---'|    \n"
    "` ' '`---'`---'`   '`---'`---^`   '`---'`---'`    \n\n"
    "You are at the control of a lunar module which is\n"
    "taking a small team of astronauts down to the moon's\n"
    "surface. In order to land safely you must slow down\n"
    "your descent, but that takes fuel and you have only\n"
    "a limited supply.\n\n";

const char *help_text =
    "Your computer will tell you your starting height,\n"
    "speed and fuel supply and ask how much fuel you wish\n"
    "to burn. It will then work out your new height and\n"
    "speed. A burn of 5 will keep your speed constant. A\n"
    "higher number will reduce it. Try to have your speed\n"
    "as close to zero as you can when you land. Can you\n"
    "land safely on the moon ?\n\n";

void clearScreen() {
    printf("\e[1;1H\e[2J");
}

char readChar() {
    char *p;
    char s[1];

    p = s;
    while((*p++ = getchar()) != '\n');
    return s[0];
}

char menu() {
    char c;
    puts("\t(N)ew game");
    puts("\t(H)elp");
    puts("\t(Q)uit");
    do {
        printf("\n\tYour choice? ");
        fflush(stdin);
        c = readChar();
    } while(c != 'N' && c != 'n' && c != 'H' && c != 'h' && c != 'Q' && c != 'q');
    return(c);
}

void new_game() {
    int time = 0;
    float height = 500;
    float velocity = 50;
    int fuel = 120;
    int burn;
    float v1;
    bool game_run = true;

    while(game_run) {
        printf("\nTime: %d Height: %.2f\n",time,height);
        printf("Velocity: %.2f Fuel: %d\n",velocity,fuel);

        if(fuel > 0) {
            printf("\nBurn? (0-30) :");
            scanf("%d",&burn);
            if(burn < 0) {
                burn = 0;
            }
            if(burn > 30) {
                burn = 30;
            }
        }
        if(burn > fuel) {
            burn = fuel;
        }
        v1 = velocity - burn + 5;
        fuel = fuel - burn;

        if(((v1 + velocity) / 2) < height) {
            height = height - (v1 + velocity) / 2;
            time = time + 1;
            velocity = v1;
        } else {
            v1 = velocity + (5 - burn) * height / velocity;
            puts("\n");
            if(v1 > 5) {
                puts("You crashed-All dead");
            }
            if(v1 > 1 && v1 <= 5) {
                puts("ok-but some injuries");
            }
            if(v1 <= 1) {
                puts("Good landing.");
            }
            puts("\n");
            game_run = false;
        }
    }
}

int main() {
    char choice;

    clearScreen();
    puts(title);
    while(1) {
        choice = menu();
        switch(choice) {
            case 'n': case 'N':
                new_game();
                break;
            case 'h': case 'H':
                printf("%s",help_text);
                break;
            case 'q': case 'Q':
                return 0;
                break;
        }
    }    
}