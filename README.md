```    
,-.-.               |                  |          
| | |,---.,---.,---.|    ,---.,---.,---|,---.,---.
| | ||   ||   ||   ||    ,---||   ||   ||---'|    
` ' '`---'`---'`   '`---'`---^`   '`---'`---'`    
```

This game is an adaptation of the Moonlander program from the book "Computer Spacegames" (Osborne Publishing Ltd - 1982). The game has been rewritten in C for Linux with the GNU compiler.
